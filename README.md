# README #

## DONE

Coroutine entities to be defined:

* `Scope`. This is something that delimits the lifetime of a coroutine.
* `Context`. Consists of several elements: Dispatcher, Job and error handler (CoroutineExceptionHandler). Without a
CoroutineExceptionHandler the app will crash if an exception is thrown in one of the coroutines.
* `Job`. An interface to the launched coroutine.
* `Dispatchers`. Defines thread pools to launch your Kotlin Coroutines in.

### What I found interesting

1. When we pass `Dispatchers.Default` or `Dispatchers.Main` to a constructor of `CoroutineScope`, what does it mean?

`"thread:" + Thread.currentThread().name` from `requestImage(address: String, displayName: String)` returned `thread:main` in both cases

**ANSWER:**

Because `coroutineScope2.launch` was called with `Dispatchers.Main`:
```
coroutineScope2.launch(Dispatchers.Main) { }
```
If `coroutineScope2.launch() { }` is called, then the `Dispatcher` from the scope is used.

### Rx Java and Kotlin Flow Operators Comparison Chart

 | RxJava     | Kotlin Flow
 | -----------|---------|
 | subscribe  | collect |
 | concatMap  |  |
 | interval  |  |
 | combineLatest  |  |
 | map        | map     |
 | filter     | filter  |
 | switchMap  | flatMapLatest  |
 | subscribeOn | scope.launch(Dispatchers.Default) |
 | Task cancellation by dispose() | Task cancellation by job.cancel() |
 | observeOn - affects **subsequent** operators | flowOn - affects **preceding** operators |

## TODO

For RxJava on one side and Coroutines/KotlinFlow on the other side compare:

* Unsubscribing on cleaning up on a particular thread (unSubscribeOn). Application: unSubscribeOn(hideProgressBar(), now the progress bar
 is not hidden after the get original or get large job is cancelled.
* Continuation. This is where a coroutine stores its state.
* Testing
* What exactly is suspended? suspend at the given point while testing if possible.
* PERFORMANCE when map, flatMap, concatMap, switchMap, etc. are used. Migrate our code to Kotlin Flow if it beats RxJava at performance.
* As per https://www.raywenderlich.com/9799571-kotlin-flow-for-android-getting-started#toc-anchor-004 channels are hot. But then why
no item emitted earlier is lost when:
    1. "Send to Channel" is pressed.
    2. "Listen to Channel" is pressed afterwards.
