package ru.maksim.kotlin.flow

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.coroutines).setOnClickListener {
            startActivity(Intent(this, CoroutinesActivity::class.java))
        }

        findViewById<Button>(R.id.kotlin_flow).setOnClickListener {
            startActivity(Intent(this, KotlinFlowSampleActivity::class.java))
        }

        findViewById<Button>(R.id.rxjava).setOnClickListener {
            startActivity(Intent(this, RxJavaActivity::class.java))
        }
    }
}