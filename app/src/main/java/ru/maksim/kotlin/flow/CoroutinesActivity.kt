package ru.maksim.kotlin.flow

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*
import java.net.URL

class CoroutinesActivity : AppCompatActivity() {

    companion object {
        const val TAG = "CoroutinesActivityTAG"
    }

    private var imageJob: Job? = null
    private var imageJobLargest: Job? = null

    // just a piece of work to be done
    private val parentJob = Job()
    private val coroutineExceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            showErrorToast()
            Log.e(TAG, "error while loading", throwable.cause)
        }

    private fun showErrorToast() {
        Toast.makeText(this@CoroutinesActivity, R.string.error_while_loading, Toast.LENGTH_LONG).show()
    }

    // scope is used to handle the lifecycle of the coroutines.
    private val coroutineScope2 = CoroutineScope(
        // The contexts and their elements are a set of rules each
        // Kotlin Coroutine has to adhere to.
        Dispatchers.Main + parentJob + coroutineExceptionHandler
    )

    private lateinit var progressBar: ProgressBar
    private lateinit var snowFilterImage: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coroutines)

        progressBar = findViewById(R.id.original_bitmap_loading_progress)
        snowFilterImage = findViewById(R.id.filtered_bitmap)

        findViewById<Button>(R.id.get_original_bitmap_async).setOnClickListener {
            imageJob = coroutineScope2.launch(Dispatchers.Main) {
                requestImage(
                    address = "https://koenig-media.raywenderlich.com/uploads/2019/01/android_rw_flag_2.png",
                    displayName = "the original"
                )
            }
        }

        findViewById<Button>(R.id.cancel_get_original_bitmap_async).setOnClickListener {
            imageJob?.cancel()
        }

        findViewById<Button>(R.id.get_largest_bitmap_async).setOnClickListener {
            imageJobLargest = coroutineScope2.launch(Dispatchers.Main) {
                requestImage(
                    address = "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/LARGE_elevation.jpg/2560px-LARGE_elevation.jpg",
                    displayName = "the largest image"
                )
            }
        }

        findViewById<Button>(R.id.cancel_largest_bitmap_async).setOnClickListener {
            imageJobLargest?.cancel()
        }

        findViewById<Button>(R.id.run_blocking).setOnClickListener {
            runBlocking {
                launch {
                    showToast(R.string.toast_1)
                    requestImage(
                        address = "https://koenig-media.raywenderlich.com/uploads/2019/01/android_rw_flag_2.png",
                        displayName = "the original"
                    )
                    showToast(R.string.toast_2)
                }
            }
        }
    }

    private fun showToast(@StringRes toastRes: Int) {
        Toast.makeText(this@CoroutinesActivity, toastRes, Toast.LENGTH_SHORT).show()
    }

    private suspend fun requestImage(address: String, displayName: String) {
        resetImage()
        displayProgressBar()
        Log.d(TAG, "requestImage thread: ${Thread.currentThread().name}")
        Log.d(TAG, "$displayName: before fetching")
        val originalBitmap = getOriginalBitmapAsync(address)
        Log.d(TAG, "$displayName: start filtering")
        val filteredBitmap = loadSnowFilterAsync(originalBitmap)
        Log.d(TAG, "$displayName: finished filtering")
        displayImage(filteredBitmap)
    }

    private suspend fun displayProgressBar() { // TODO: can it be "suspended without blocking the current thread"?
        // if it's suspended, what will happen to requestImage?
        withContext(Dispatchers.Main) {
            progressBar.visibility = View.VISIBLE
        }
    }

    private suspend fun getOriginalBitmapAsync(urlAddress: String): Bitmap {
        return withContext(Dispatchers.IO) {
            URL(urlAddress).openStream().use {
                BitmapFactory.decodeStream(it)
            }
        }
    }

    private suspend fun loadSnowFilterAsync(originalBitmap: Bitmap): Bitmap {
        return withContext(Dispatchers.Default) {
            SnowFilter.applySnowEffect(originalBitmap)
        }
    }

    private fun displayImage(snowFilterBitmap: Bitmap) {
        progressBar.visibility = View.GONE
        snowFilterImage.setImageBitmap(snowFilterBitmap)
    }

    private suspend fun resetImage() {
        withContext(Dispatchers.Main) {
            snowFilterImage.setImageBitmap(null)
        }
    }

    override fun onDestroy() {
        parentJob.cancel()
        super.onDestroy()
    }
}