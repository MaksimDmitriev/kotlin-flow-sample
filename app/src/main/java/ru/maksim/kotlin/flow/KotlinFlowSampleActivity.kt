package ru.maksim.kotlin.flow

import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import java.util.*

class KotlinFlowSampleActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "KotlinFlowActivityTAG"
        private const val DELAY = 1_000L
    }

    private val channel = Channel<Int>()

    private val namesFlow = flow {
        val names = listOf("Jody", "Steve", "Lance", "Fred", "Joe", "Fionna")
        for (name in names) {
            delay(DELAY)
            emit(name)
        }
    }
    private var kotlinFlowJob: Job? = null
    private var yieldValuesJob: Job? = null

    // just a piece of work to be done
    private val parentJob = Job()
    private val coroutineExceptionHandler: CoroutineExceptionHandler =
        CoroutineExceptionHandler { _, throwable ->
            showErrorToast()
            Log.e(CoroutinesActivity.TAG, "error while loading", throwable.cause)
        }

    private fun showErrorToast() {
        Toast.makeText(this@KotlinFlowSampleActivity, R.string.error_while_loading, Toast.LENGTH_LONG).show()
    }

    // scope is used to handle the lifecycle of the coroutines.
    private val activityScope = CoroutineScope(
        // The contexts and their elements are a set of rules each
        // Kotlin Coroutine has to adhere to.
        Dispatchers.Main + parentJob + coroutineExceptionHandler
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_flow)

        findViewById<Button>(R.id.start_kotlin_flow).setOnClickListener {
            kotlinFlowJob = activityScope.launch(Dispatchers.Main) {
                namesFlow
                    .map { name ->
                        Log.d(TAG, "toLowerCase on thread ${Thread.currentThread().name} id=${Thread.currentThread().id}")
                        name.toLowerCase(Locale.ENGLISH)
                    }
                    .onEach {
                        Log.d(TAG, "flowOn IO done, name=$it on thread ${Thread.currentThread().name} id=${Thread.currentThread().id}")
                    }
                    .flowOn(Dispatchers.IO)
                    .onEach {
                        Log.d(TAG, "flowOn Main done, name=$it on thread ${Thread.currentThread().name} id=${Thread.currentThread().id}")
                    }
                    .flowOn(Dispatchers.Default)
                    .map { name -> name.toUpperCase(Locale.ENGLISH) }
                    .filter { it.isNotEmpty() && it[0] != 'F' }
                    .collect {
                        Log.d(TAG, "name=$it on thread ${Thread.currentThread().name} id=${Thread.currentThread().id}")
                    }
            }
        }

        findViewById<Button>(R.id.yield_values).setOnClickListener {
            yieldValuesJob = activityScope.launch(Dispatchers.Default) {
                processValues()
            }
        }

        findViewById<Button>(R.id.send_to_channel).setOnClickListener {
            activityScope.launch(Dispatchers.Default) {
                for (x in 1..5) {
                    delay(DELAY)
                    channel.send(x * x)
                    Log.d(TAG, "x=$x sent to channel on thread ${Thread.currentThread().name}")
                }
                channel.close() // we're done sending
            }
        }

        findViewById<Button>(R.id.listen_to_channel).setOnClickListener {
            activityScope.launch(Dispatchers.Default) {
                for (y in channel) {
                    Log.d(TAG, "y=$y received from channel on thread ${Thread.currentThread().name}")
                }
            }
        }
    }

    private suspend fun getValues(): Sequence<Int> = sequence {
        Thread.sleep(DELAY)
        yield(1)
        Thread.sleep(DELAY)
        yield(2)
        Thread.sleep(DELAY)
        yield(3)
    }

    private suspend fun processValues() {
        val values = getValues()
        for (value in values) {
            Log.d(TAG, "value=$value on thread ${Thread.currentThread().name}")
        }
    }
}